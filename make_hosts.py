import sys
from pathlib import Path


TEMPLATE = """127.0.0.1 localhost
{is1} is1
{is2} is2
{is3} is3

# The following lines are desirable for IPv6 capable hosts
::1 ip6-localhost ip6-loopback
fe00::0 ip6-localnet
ff00::0 ip6-mcastprefix
ff02::1 ip6-allnodes
ff02::2 ip6-allrouters
ff02::3 ip6-allhosts
"""

SSH = """
Host is1
  HostName {is1}
  User isucon
  IdentityFile {key}
Host is2
  HostName {is2}
  User isucon
  IdentityFile {key}
HOST is3
  HostName {is3}
  User isucon
  IdentityFile {key}
"""

def main():
    config = Path(__file__).parent / 'config'
    is1, is2, is3, *keys = sys.argv[1:]
    for i in range(1, 4):
        path = config / f'is{i}' / 'etc'
        path.mkdir(parents=True, exist_ok=True)
        data = TEMPLATE.format(
            is1=is1 if i != 1 else '127.0.0.1',
            is2=is2 if i != 2 else '127.0.0.1',
            is3=is2 if i != 3 else '127.0.0.1',
        )
        with (path / 'hosts').open('w') as f:
            f.write(data)

    for key in keys:
        print(SSH.format(
            is1=is1,
            is2=is2,
            is3=is3,
            key=key,
        ))


if __name__ == '__main__':
    main()
