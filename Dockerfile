FROM rust:1.54.0-buster AS builder

WORKDIR /app

COPY Cargo.* .
COPY src src

RUN --mount=type=cache,target=/usr/local/cargo/registry \
    --mount=type=cache,target=/app/target \
    cargo build --release && cp /app/target/release/isucondition /

FROM scratch

COPY --from=builder /isucondition /
