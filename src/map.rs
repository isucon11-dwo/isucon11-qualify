use rustc_hash::{FxHashMap, FxHasher};
use std::hash::{Hash, Hasher};
use tokio::sync::{RwLock, RwLockReadGuard, RwLockWriteGuard};

const DIV: u64 = 1024;
// (e * 2 ** 64) & (2 ** 64 - 1)
const SHUFFLE: u64 = 13249961062380150784;

pub type Map<K, V> = FxHashMap<K, V>;

pub struct LockedHashMap<K, V> {
    maps: Vec<RwLock<Map<K, V>>>,
}

impl<K, V> LockedHashMap<K, V>
where
    K: Hash,
{
    pub fn new() -> LockedHashMap<K, V> {
        let mut maps = Vec::with_capacity(DIV as usize);
        for _ in 0..DIV {
            maps.push(RwLock::new(FxHashMap::default()));
        }
        LockedHashMap { maps }
    }

    fn key_hash(key: &K) -> u64 {
        let mut hasher = FxHasher::default();
        key.hash(&mut hasher);
        hasher.finish()
    }

    pub async fn read(&self, key: &K) -> RwLockReadGuard<'_, Map<K, V>> {
        let key = Self::key_hash(key);
        let d = key.wrapping_mul(SHUFFLE) & (DIV - 1);
        self.maps[d as usize].read().await
    }

    pub async fn write(&self, key: &K) -> RwLockWriteGuard<'_, Map<K, V>> {
        let key = Self::key_hash(key);
        let d = key.wrapping_mul(SHUFFLE) & (DIV - 1);
        self.maps[d as usize].write().await
    }

    pub async fn clear(&self) {
        for lock in self.maps.iter() {
            lock.write().await.clear();
        }
    }
}
