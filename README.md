# isucon11-qualify

## リンク

* [ISUCON11 ポータル](https://portal.isucon.net/)
* [ISUCON11 予選レギュレーション](https://isucon.net/archives/55854734.html)
* [ISUCON11 公式 Discord](https://discord.com/channels/857222988088737803/857222988368707589)
* [ISUCON11 当日マニュアル](https://gist.github.com/ockie1729/53589a0e8c979198b6231d8599153c70)
* [やることリスト](https://docs.google.com/spreadsheets/d/1zviVM41uatS-5PI5fGMAqqTrEUM19rof30QdbagsQO8/edit#gid=0)

## ツール

### make_hosts.py

```
$ python3 make_hosts.py 10.100.20.1 10.100.30.2 10.100.40.3 ~/.ssh/id_rsa

Host is1
  HostName 10.100.20.1
  User isucon
  IdentityFile /Users/daiju/.ssh/id_rsa
Host is2
  HostName 10.100.30.2
  User isucon
  IdentityFile /Users/daiju/.ssh/id_rsa
HOST is3
  HostName 10.100.40.3
  User isucon
  IdentityFile /Users/daiju/.ssh/id_rsa

$ cat config/is2/etc/hosts
127.0.0.1 localhost
10.100.20.1 is1
127.0.0.1 is2
10.100.30.2 is3

# The following lines are desirable for IPv6 capable hosts
::1 ip6-localhost ip6-loopback
fe00::0 ip6-localnet
ff00::0 ip6-mcastprefix
ff02::1 ip6-allnodes
ff02::2 ip6-allrouters
ff02::3 ip6-allhosts
```

## API

```

// 初期化
POST /initialize

// サインアップ・サインイン
POST /api/auth                      post_authentication
// サインアウト
POST /api/signout                   post_signout
// サインインしている自分自身の情報を取得
GET  /api/user/me                   get_me
// ISU の一覧を取得
GET  /api/isu                       get_isu_list
// ISU を登録
POST /api/isu                       post_isu
// ISU の情報を取得
GET  /api/isu/{jia_isu_uuid}        get_isu_id
// ISU のアイコンを取得
GET  /api/isu/{jia_isu_uuid}/icon   get_isu_icon
// ISU のコンディショングラフ描画のための情報を取得
GET  /api/isu/{jia_isu_uuid}/graph  get_isu_graph
// ISU のコンディションを取得
GET  /api/condition/{jia_isu_uuid}  get_isu_conditions
// ISU の性格毎の最新のコンディション情報
GET  /api/trend                     get_trend
// ISU からのコンディションを受け取る
POST /api/condition/{jia_isu_uuid}  post_isu_condition

// その他
GET /                               get_index
GET /isu/{jia_isu_uuid}             get_index
GET /isu/{jia_isu_uuid}/condition   get_index
GET /isu/{jia_isu_uuid}/graph       get_index
GET /register                       get_index
GET /assets                         ../public/assets
```